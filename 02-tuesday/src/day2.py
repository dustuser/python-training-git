# TODO struct
import struct

# TODO bytearray (no import needed)


# ----------------------------------------------------------------------
# HTTP

# Reference http://docs.atlassian.com/atlassian-confluence/REST/3.2/

CONFLUENCE_REST_BASE = 'https://confluence.dusthq.dust-inc.com:8443/rest/prototype/1'

import urllib
urlf = urllib.urlopen(CONFLUENCE_REST_BASE+'/search?query=IPMG_UG&spaceKey=DOC')
urldata = urlf.read()

# Need more control?
#import urllib2


# XML

import re
from xml.parsers.expat import ExpatError
from xml.dom.minidom import parseString
results_xml = parseString(urldata)
result_tags = results_xml.getElementsByTagName('result')
result_ids = [int(r.getAttribute('id')) for r in result_tags]

def check_page(page_id):
    pagedata = urllib.urlopen(CONFLUENCE_REST_BASE+'/content/%d' % page_id).read()
    page_xml = parseString(pagedata)

    body_data = page_xml.getElementsByTagName('body')[0].firstChild.wholeText
    body_better = re.sub('&[a-z]*', ' ', body_data)
    body_wrapped = '''<root xmlns = "http://default-namespace.org/" 
      xmlns:ac = "https://confluence.dusthq.dust-inc.com:8443/"
      xmlns:ri = "https://confluence.dusthq.dust-inc.com:8443/">''' + body_better + '''</root>'''
    try:
        body_xml = parseString(body_wrapped)
        macros = body_xml.getElementsByTagName('ac:macro')
        #return [m.getAttribute('ac:name') for m in macros]
        for m in macros:    
            if m.getAttribute('ac:name') == 'include':
                print 'Page', page_id, 'includes', m.firstChild.toprettyxml()
    except ExpatError as ex:
        print ex.message
        print body_better
    except UnicodeEncodeError:
        print 'Page', page_id, 'has parse errors'
    

for p in result_ids:
    check_page(p)

# TODO XML-RPC
