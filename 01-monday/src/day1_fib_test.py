#!/usr/bin/env python


import unittest

from day1_fib import fib

FIB_SEQ = (1, 1, 2, 3, 5, 8, 13, 21, 34)

class FibTest(unittest.TestCase):
    def test_initial(self):
        self.assertEquals(fib(1), 1)
        self.assertEquals(fib(2), 1)

    def test_alot(self):
        for n, expected in enumerate(FIB_SEQ):
            self.assertEquals(fib(n+1), expected)
