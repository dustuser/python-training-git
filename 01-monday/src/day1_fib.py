#!/usr/bin/env python

def fib(n):
    a, b = 1, 1
    for i in range(n):
        a, b = b, a+b
    return a

def test_fib():
    assert(fib(1) == 1)
    assert(fib(2) == 1)
    assert(fib(3) == 2)
    assert(fib(4) == 3)
    assert(fib(5) == 5)


def better_test_fib():
    n = 1
    for expected in (1, 1, 2, 3, 5, 8):
        assert(fib(n) == expected)
        n += 1

        
def even_better_test_fib():
    for n, expected in enumerate((1, 1, 2, 3, 5, 8, 13)):
        assert(fib(n+1) == expected)
