#
# Welcome to Python Week Day 1!
#

# ----------------------------------------------------------------------
# Installation
# - Python 2.7
# - setuptools
# - pywin32
# - pip

# Run python from the command line
# >>> 

# Run IDLE
# >>> 

# ----------------------------------------------------------------------
# Numbers

x = 23
x ** 2 + 3x
x ** 2 + 3*x
import math
math.sqrt(x**2 + 3*x)

# ----------------------------------------------------------------------
# Strings

s = 'Something'
longstr = '''You can have
    multiple lines.
    If you really want to.'''

# - Formatting: % operator, format()
"This is a report: x=%d, log(x)=%f" % (4, math.log(4))
"This is a report: x={0}, log(x)={1}".format(4, math.log(4))

# ----------------------------------------------------------------------
# ifs, loops
# - Indentation

for i in range(10):
    print i

primes = [2, 3, 5, 7, 11, 13, 17, 19, 23]
for el in primes:
    print el**2
for i, p in enumerate(primes):
    print i, '->', p


# ----------------------------------------------------------------------
# Lists, Dicts will be covered later


# ----------------------------------------------------------------------
# functions

def f(x):
    '''Returns x squared'''    
    return x * x

f(13)

g = f
g(15)

d = {'sq': f}
d['sq'](16)

import math
def test_prime(n):
    if not (n % 2):
        return False
    for i in range(3, int(math.sqrt(n))+1, 2):
        if not (n % i):
            return False
    return True


# ----------------------------------------------------------------------
# classes

class PrimeFinder(object):
    def __init__(self):
        self.primes = [2, 3, 5, 7, 11]

    def is_prime(self, n):
        'Returns whether n is prime'
        max_prime = max(self.primes)
        if n < max_prime:
            return n in self.primes
        elif n < max_prime ** 2:
            for p in self.primes:
                if (n % p) == 0:
                    return False
            return True
        else:
            raise NotImplementedError("can't compute primes yet")

# ----------------------------------------------------------------------
# Interactive help

help(list)

class Foo(object):
    def method_a(self, x):
        '''Returns x squared'''
        return x * x

f = Foo()
help(f)
help(f.method_a)

# ----------------------------------------------------------------------
# Modules - load a file and interact with it
# (see day1_fib.py) 


# ----------------------------------------------------------------------
# Packages
# $ easy_install networkx
# explain sys.path
